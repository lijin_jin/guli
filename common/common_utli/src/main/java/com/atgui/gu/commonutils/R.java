package com.atgui.gu.commonutils;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class R {

    @ApiModelProperty(value = "是否成功")
    private boolean success;

    @ApiModelProperty(value = "返回值")
    private Integer code;

    @ApiModelProperty(value = "返回信息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private Map<String,Object> date = new HashMap<String,Object>();

    //私有化构造方法
    private R(){ }

    //成功的静态方法
    public static R ok(){
        R r = new R();

        r.setSuccess(true);
        r.setCode(ResultCode.SUCCESS);
        r.setMessage("成功");
        return r;

    }

    //失败的静态方法
    public static R error(){
        R r = new R();

        r.setSuccess(false);
        r.setCode(ResultCode.ERROR);
        r.setMessage("失败");
        return r;
    }


    public R success(boolean success){
        this.setSuccess(success);
        return this;
    }

    public R code(Integer code){
       this.setCode(code);
        return this;
    }

    public R message(String message){
        this.setMessage(message);

        return this;
    }

    public R data(String key,Object value){
        this.date.put(key, value);
        return this;

    }

    public R date(Map<String,Object> map){
        this.date(map);
        return this;

    }
}
