package com.atgui.gu.servicebase.exceptionhandler;



import com.atgui.gu.commonutils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
//@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public R error(Exception e){

        e.fillInStackTrace();
       return R.error().message("全局处理异常");
    }

    @ResponseBody
    @ExceptionHandler(GuLIExceptionHandler.class)
    public R error(GuLIExceptionHandler e){
        log.error(e.getMessage());
        e.fillInStackTrace();
        return R.error().code(e.getCode()).message(e.getMsg());
    }
}
