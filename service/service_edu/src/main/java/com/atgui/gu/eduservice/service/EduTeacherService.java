package com.atgui.gu.eduservice.service;

import com.atgui.gu.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author testjava
 * @since 2022-03-25
 */
public interface EduTeacherService extends IService<EduTeacher> {

}
