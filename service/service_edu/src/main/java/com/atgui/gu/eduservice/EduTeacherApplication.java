package com.atgui.gu.eduservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.atgui.gu")
public class EduTeacherApplication {

    public static void main(String[] args) {

        SpringApplication.run(EduTeacherApplication.class,args );
    }
}
