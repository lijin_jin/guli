package com.atgui.gu.eduservice.controller;


import com.atgui.gu.commonutils.R;
import com.atgui.gu.eduservice.entity.EduTeacher;
import com.atgui.gu.eduservice.entity.vo.TeacherQuery;
import com.atgui.gu.eduservice.service.EduTeacherService;
import com.atgui.gu.servicebase.exceptionhandler.GuLIExceptionHandler;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2022-03-25
 */
@RestController
@RequestMapping("/eduservice/edu-teacher")
@CrossOrigin
public class EduTeacherController {

    @Autowired
    private EduTeacherService eduTeacherService;

    @GetMapping("/findAll")
    @ApiOperation(value = "所有讲师列表")
    public R findAllTeacher(){

        List<EduTeacher> list = eduTeacherService.list(null);

        return R.ok().data("items", list);

    }

    @ApiOperation(value = "逻辑删除讲师")
    @DeleteMapping("/{id}")
    public R removeTeacher(@ApiParam(name = "id",value = "讲师id",required = true)  @PathVariable("id") String id){

        boolean flag = eduTeacherService.removeById(id);
        if (flag){
            return R.ok();
        }
        else {
            return R.error();
        }
    }

    //分页查询
    @GetMapping("pageTeacher/{current}/{limit}")
    @ApiOperation(value = "分页查询讲师")
    public R pageListTeacher(@PathVariable("current") Long current,@PathVariable("limit") Long limit){
        
        //创建page对象
        Page<EduTeacher> pageTeacher = new Page<>(current,limit);

        //调用方法实现分页
        IPage<EduTeacher> page = eduTeacherService.page(pageTeacher, null);

        long total = pageTeacher.getTotal();
        List<EduTeacher> records = pageTeacher.getRecords();

//        //第二种写法
//        Map map = new HashMap<>();
//        map.put("total", total);
//        map.put("rows", records);
//        return R.ok().date(map);

        return R.ok().data("total", total).data("rows", records);
    }


    @PostMapping("/pageTeacherCondition/{current}/{limit}")
    public R pageTeacherCondition1(@PathVariable Long current, @PathVariable Long limit,
                                  @RequestBody(required = false) TeacherQuery teacherQuery){

        //创建page对象
        Page<EduTeacher> pageTeacher = new Page<>(current, limit);

        try {
            int i =10/0;
        }catch (Exception e){
            throw new GuLIExceptionHandler(20001,"自定义异常");
        }


        //构建条件
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();

        //获取用户的条件数据
        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        String begin = teacherQuery.getBegin();
        String end = teacherQuery.getEnd();

        //判断有数据 如果有进行拼接
        if(!StringUtils.isEmpty(name)) {
            //构建条件
            wrapper.like("name",name);
        }
        if(!StringUtils.isEmpty(level)) {
            wrapper.eq("level",level);
        }
        if(!StringUtils.isEmpty(begin)) {
            wrapper.ge("gmt_create",begin);
        }
        if(!StringUtils.isEmpty(end)) {
            wrapper.le("gmt_create",end);
        }
        //分页查询结果
        eduTeacherService.page(pageTeacher, wrapper);
        //总记录数
        long total = pageTeacher.getTotal();
        //数据集合
        List<EduTeacher> records = pageTeacher.getRecords();

        return R.ok().data("total", total).data("rows", records);

    }


    @PostMapping("/addTeacher")
    public R addTeacher(@RequestBody EduTeacher eduTeacher){

        boolean save = eduTeacherService.save(eduTeacher);
        if (save){
            return R.ok();
        }else {
            return R.error();
        }

    }

    @GetMapping("/getTeacher/{id}")
    public R getTeacher(@PathVariable("id") String id){
        EduTeacher eduTeacher = eduTeacherService.getById(id);

        return R.ok().data("teacher", eduTeacher);

    }

    @PutMapping("/updateTeacher")
    public R updateTeacher(@RequestBody EduTeacher eduTeacher){
        boolean flag = eduTeacherService.updateById(eduTeacher);
        if (flag){
            return R.ok();
        }else {
            return R.error();
        }

    }

}

